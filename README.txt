#How to run my code for Assignment 2#

##Setup##
The first thing you will want to do is clone down my public repo for this
project which includes an eclipse project with my program files.  You can
clone it with this git command:

git clone https://mkimmet@bitbucket.org/mkimmet/ml-project-2.git
site at: https://bitbucket.org/mkimmet/ml-project-2/src/master/

My project uses ABAGAIL and xchart-3.6.1. XChart is used for creating the charts
and the jar file is included in the project so you shouldn't have to do anything
to get it xchart to work.  My project is an eclipse IDE project so you will
probably want to use eclipse and install ABAGAIL with eclipse by following
these directions:

https://abagail.readthedocs.io/en/latest/faq.html#how-to-use-abagail-with-eclipse

##Datasets##
All my training and test datasets are included in the repo so you shouldn't
have to do anything special for the project to access the data sets.

##Running my program##
Once you have the project cloned and ABAGAIL setup then in eclipse open up
my Main.java file.  Everything runs from the main function.  If you run the
project it will run all 3 optimization problems, output the results to the
command line and generate charts one folder up from your project.

It will also run the default settings for the Neural Network weighting for 
all 3 algorithms, training against all my different training sizes.  It 
will then evaluate the training sets, outputting the accuracy results and
then it will evaluate the validation test set and output it's results.
NOTE this might take a very long time!

To get the results I got for this assignment I tweaked many settings in
the main.java file and so you would need to tweak and re run the program
multiple times in order to get all the results that I got.  Here are some
of the settings that I changed:

line 73 - hiddenLayer and trainingIterations
private static int inputLayer = 23, hiddenLayer = 12, outputLayer = 1, trainingIterations = 1000;

lines 168-169 settings for SA and GA
oa[1] = new SimulatedAnnealing(1E11, .95, nnop[1]);
oa[2] = new StandardGeneticAlgorithm(300, 200, 22, nnop[2]);

The fixed iterations for the various RO problems like:
FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 100000);

The N factors for the various RO problems like:
int[] Ns = new int[] {10,20,50,100};

The number of times to run the RO problems like:
int[] iterations = new int[] { 500 };
