import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.style.markers.SeriesMarkers;

/**
 * Simple wrapper for the xChart system to take lines and create a chart
 * @author markkimmet
 *
 */
public class Charty {
	public Charty() {
        RHCline.title = "RHC";
        SAline.title = "SA";
        GAline.title = "GA";
        MIMICline.title = "MIMIC";
        RHCTimeline.title = "RHC";
        SATimeline.title = "SA";
        GATimeline.title = "GA";
        MIMICTimeline.title = "MIMIC";
	}
	public List<Line> lines = new ArrayList<Line>();
	public List<Line> Timelines = new ArrayList<Line>();
	public Line RHCline = new Line();
	public Line SAline = new Line();
	public Line GAline = new Line();
	public Line MIMICline = new Line();
  	 
	public Line RHCTimeline = new Line();
	public Line SATimeline = new Line();
	public Line GATimeline = new Line();
	public Line MIMICTimeline = new Line();
	
	public void addLines() {
		lines.add(RHCline);
        lines.add(SAline);
        lines.add(GAline);
        lines.add(MIMICline);
        Timelines.add(RHCTimeline);
        Timelines.add(SATimeline);
        Timelines.add(GATimeline);
        Timelines.add(MIMICTimeline);
	}
	
	public void addPointR(double x, double y) {
		RHCline.xData.add(x);
		RHCline.yData.add(y);
	}
	public void addTimePointR(double x, double y) {
		RHCTimeline.xData.add(x);
		RHCTimeline.yData.add(y);
	}
	
	public void addPointS(double x, double y) {
		SAline.xData.add(x);
		SAline.yData.add(y);
	}
	public void addTimePointS(double x, double y) {
		SATimeline.xData.add(x);
		SATimeline.yData.add(y);
	}
	
	public void addPointG(double x, double y) {
		GAline.xData.add(x);
		GAline.yData.add(y);
	}
	public void addTimePointG(double x, double y) {
		GATimeline.xData.add(x);
		GATimeline.yData.add(y);
	}
	
	public void addPointM(double x, double y) {
		MIMICline.xData.add(x);
		MIMICline.yData.add(y);
	}
	public void addTimePointM(double x, double y) {
		MIMICTimeline.xData.add(x);
		MIMICTimeline.yData.add(y);
	}
	
	
	public void chart(List<Line> lines, String filename, String title, String xAxisTitle, String yAxisTitle) throws IOException {
        XYChart chart = new XYChart(500, 400);
        chart.setTitle(title);
        chart.setXAxisTitle(xAxisTitle);
        chart.setYAxisTitle(yAxisTitle);
        List<XYSeries> series = new ArrayList<XYSeries>();
        for(Line line: lines){
        	series.add(chart.addSeries(line.title, line.getXData(), line.getYData()));
        }
        
        for(XYSeries s: series){
        	s.setMarker(SeriesMarkers.CIRCLE);
        }
        BitmapEncoder.saveBitmapWithDPI(chart, "../" + filename, BitmapFormat.PNG, 300);
	}
}
