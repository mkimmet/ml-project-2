import java.util.ArrayList;
import java.util.List;

/**
 * Simple class for a line for use with Charty
 * @author markkimmet
 *
 */
public class Line {
	public List<Double> xData = new ArrayList<Double>();
	public List<Double> yData = new ArrayList<Double>();
    public String title;
    public double[] getXData() {
    	return this.getdoubles(xData);
    }
    public double[] getYData() {
    	return this.getdoubles(yData);
    }
     
   //Get double[] from Doubles[] from: https://stackoverflow.com/questions/6018267/how-to-cast-from-listdouble-to-double-in-java
 	public double[] getdoubles(List<Double> doubles) {
 		double[] target = new double[doubles.size()];
 		 for (int i = 0; i < target.length; i++) {
 		    target[i] = doubles.get(i);
 		 }
 		 return target;
 	}
 	
 	
 	
}
