import dist.*;
import opt.*;
import opt.example.*;
import opt.ga.*;
import shared.*;
import func.nn.BackpropNetworkBuilder;
import func.nn.activation.LogisticSigmoid;
import func.nn.backprop.*;

import java.util.*;
import java.io.*;
import java.text.*;

import java.util.Arrays;
import java.util.Random;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.VectorGraphicsEncoder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.markers.SeriesMarkers;

import opt.ga.NQueensFitnessFunction;
import dist.DiscreteDependencyTree;
import dist.DiscretePermutationDistribution;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.SwapNeighbor;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.SingleCrossOver;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.SwapMutation;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

public class Main {
	
    //private static Instance[] instances = initializeInstances(); //abalone
    private static Instance[] instances = initializeInstancesForPhishing(7075,"src/phishing-train-80-23attr.txt",23); //phishing
    private static Instance[] instancesTest = initializeInstancesForPhishing(1769,"src/phishing-CV-20-lessattr.txt",23); //phishing
    private static Instance[] instancesTest1 = initializeInstancesForPhishing(5660,"src/phishing-train-80-80-lessattr.txt",23);
    private static Instance[] instancesTest2 = initializeInstancesForPhishing(3962,"src/phishing-train-80-80-70-lessattr.txt",23);
    private static Instance[] instancesTest3 = initializeInstancesForPhishing(2773,"src/phishing-train-80-80-70-70-lessattr.txt",23);
    private static Instance[] instancesTest4 = initializeInstancesForPhishing(1663,"src/phishing-train-80-80-70-70-60-lessattr.txt",23);
    private static Instance[] instancesTest5 = initializeInstancesForPhishing(997,"src/phishing-train-80-80-70-70-60-60-lessattr.txt",23);
    private static Instance[] instancesTest6 = initializeInstancesForPhishing(498,"src/phishing-train-80-80-70-70-60-60-50-lessattr.txt",23);
    private static Instance[] instancesTest7 = initializeInstancesForPhishing(249,"src/phishing-train-80-80-70-70-60-60-50-50-lessattr.txt",23);
    private static Instance[] instancesTest8 = initializeInstancesForPhishing(124,"src/phishing-train-80-80-70-70-60-60-50-50-50-lessattr.txt",23);
    //private static int inputLayer = 7, hiddenLayer = 5, outputLayer = 1, trainingIterations = 1000; //abalone

    private static List<Instance[]> ins = new ArrayList<Instance[]>();
    
    private static int inputLayer = 23, hiddenLayer = 12, outputLayer = 1, trainingIterations = 1000; //phishing
    private static BackPropagationNetworkFactory factory = new BackPropagationNetworkFactory();
    //private static BackpropNetworkBuilder propnet = new BackpropNetworkBuilder();
    private static ErrorMeasure measure = new SumOfSquaresError();

    private static DataSet set = new DataSet(instances);
    private static DataSet setTest = new DataSet(instancesTest);
    private static DataSet setTest1 = new DataSet(instancesTest1);
    private static DataSet setTest2 = new DataSet(instancesTest2);
    private static DataSet setTest3 = new DataSet(instancesTest3);
    private static DataSet setTest4 = new DataSet(instancesTest4);
    private static DataSet setTest5 = new DataSet(instancesTest5);
    private static DataSet setTest6 = new DataSet(instancesTest6);
    private static DataSet setTest7 = new DataSet(instancesTest7);
    private static DataSet setTest8 = new DataSet(instancesTest8);
    
    private static List<DataSet> sets = new ArrayList<DataSet>();
    
    private static int NumberOfTests = 3;
    private static String[] oaNames = {"RHC", "SA", "GA"};
    // private static String[] oaNames = {"RHC", "SA"};
	// private static String[] oaNames = {"GA"};
	 private static String results = "";

    private static BackPropagationNetwork networks[] = new BackPropagationNetwork[NumberOfTests];
    private static NeuralNetworkOptimizationProblem[] nnop = new NeuralNetworkOptimizationProblem[NumberOfTests];

    private static OptimizationAlgorithm[] oa = new OptimizationAlgorithm[NumberOfTests];
   

    private static DecimalFormat df = new DecimalFormat("0.000");
    
    
    /** The n value */
   
    /** The t value 
     * @throws IOException */
    

    public static void main(String[] args) throws IOException {
    	
    	Queens("NQueens");
    	TSM("N-TSM");
    	fourPeaks("N-4peaks");
    	//for(int i=0;i<3;i++) {
    		NNWeighting();
    	//}
    }
    
    public static void NNWeighting() {
    	sets.add(setTest8);
    	sets.add(setTest7);
    	sets.add(setTest6);
    	sets.add(setTest5);
    	sets.add(setTest4);
    	sets.add(setTest3);
    	sets.add(setTest2);
    	sets.add(setTest1);
    	sets.add(set);
    	//sets.add(setTest); //CV
    	String[] inNames = {
    			"80-80-70-70-60-60-50-50-50",
    			"80-80-70-70-60-60-50-50",
    			"80-80-70-70-60-60-50",
    			"80-80-70-70-60-60",
    			"80-80-70-70-60",
    			"80-80-70-70",
    			"80-80-70",
    			"80-80",
    			"80",
    			"CV"};
    	
    	ins.add(instancesTest8);
    	ins.add(instancesTest7);
    	ins.add(instancesTest6);
    	ins.add(instancesTest5);
    	ins.add(instancesTest4);
    	ins.add(instancesTest3);
    	ins.add(instancesTest2);
    	ins.add(instancesTest1);
    	ins.add(instances);
    	ins.add(instancesTest); // CV
    
    	
    	
    	for(int k=0;k<sets.size();k++) {
    	
        	//Added LogisticSigmoid to be more like Weka's unipolar sigmoid Activiation
            for(int i = 0; i < oa.length; i++) {
                networks[i] = factory.createClassificationNetwork(
                    new int[] {inputLayer, hiddenLayer, outputLayer}, new LogisticSigmoid()); //Added LogisticSigmoid
                nnop[i] = new NeuralNetworkOptimizationProblem(sets.get(k), networks[i], measure);
            }

            oa[0] = new RandomizedHillClimbing(nnop[0]);
            oa[1] = new SimulatedAnnealing(1E11, .95, nnop[1]);
            oa[2] = new StandardGeneticAlgorithm(300, 200, 22, nnop[2]);

            for(int i = 0; i < oa.length; i++) {
                double start = System.nanoTime(), end, trainingTime, testingTime, correct = 0, incorrect = 0, correct2=0,incorrect2=0;
                train(oa[i], networks[i], oaNames[i]); //trainer.train();
                end = System.nanoTime();
                trainingTime = end - start;
                trainingTime /= Math.pow(10,9);
                
                //Adding a standard rule that has momentum and learning rate to try be more like weka
                //the settings that I used in weka for my 
                StandardUpdateRule standardRule = new StandardUpdateRule(0.3,0.2); 
                networks[i].updateWeights(standardRule);
                

                Instance optimalInstance = oa[i].getOptimal();
                networks[i].setWeights(optimalInstance.getData());

                	 //TESTING TEST DATA
                	 //Changed test to use Cross Validation Dataset to be more like weka
                    double predicted, actual;
                    start = System.nanoTime();
                    for(int j = 0; j < ins.get(k).length; j++) {
                        networks[i].setInputValues(ins.get(k)[j].getData());
                        networks[i].run();

                        predicted = Double.parseDouble(ins.get(k)[j].getLabel().toString());
                        actual = Double.parseDouble(networks[i].getOutputValues().toString());

                        double trash = Math.abs(predicted - actual) < 0.5 ? correct++ : incorrect++;

                    }
                    end = System.nanoTime();
                    testingTime = end - start;
                    testingTime /= Math.pow(10,9);
                    
                    System.out.println(inNames[k] + " - Train");
                    results =  "\nResults for " + oaNames[i] + ": \nCorrectly classified " + correct + " instances." +
                                "\nIncorrectly classified " + incorrect + " instances.\nPercent correctly classified: "
                                + df.format(correct/(correct+incorrect)*100) + "%\nTraining time: " + df.format(trainingTime)
                                + " seconds\nTesting time: " + df.format(testingTime) + " seconds\n";
                    System.out.println(results);
                    System.out.println("++++++++++++++");
                   
                  //TESTING CV  
                  //Changed test to use Cross Validation Dataset to be more like weka
                    double predicted2, actual2;
                    start = System.nanoTime();
                    for(int j = 0; j < ins.get(9).length; j++) {
                        networks[i].setInputValues(ins.get(9)[j].getData());
                        networks[i].run();

                        predicted2 = Double.parseDouble(ins.get(9)[j].getLabel().toString());
                        actual2 = Double.parseDouble(networks[i].getOutputValues().toString());

                        double trash2 = Math.abs(predicted2 - actual2) < 0.5 ? correct2++ : incorrect2++;

                    }
                    end = System.nanoTime();
                    testingTime = end - start;
                    testingTime /= Math.pow(10,9);
                    
                    System.out.println(inNames[9] + " - Test");
                    results =  "\nResults for " + oaNames[i] + ": \nCorrectly classified " + correct2 + " instances." +
                                "\nIncorrectly classified " + incorrect2 + " instances.\nPercent correctly classified: "
                                + df.format(correct2/(correct2+incorrect2)*100) + "%\nTraining time: " + df.format(trainingTime)
                                + " seconds\nTesting time: " + df.format(testingTime) + " seconds\n";
                    System.out.println(results);
                    System.out.println("++++++++++++++");
                
               
            }
    		
    	}
    	
    	

        
    	System.out.println("++++++++++++++");
    	System.out.println("++++++++++++++");
    	System.out.println("++++++++++++++");
    	//double[] time = new double[]{0,0,0,0};
    	//double[] perf = new double[]{0,0,0,0};
    	//List<Double> perfList = new ArrayList<Double>();
    }
    
    public static void TSM(String fileName) throws IOException {

    	//PERFORMANCE IS INVERSE OF DISTANCE
    	//N Points: TEST Variations for time: 10,20,50,100,200
    	
        //int N = 50;
        int[] Ns = new int[] {10,20,50,100};
        Charty chartit = new Charty();
        
        int[] iterations = new int[] {5};
    	 
        for(int N: Ns) {
        	for(int ii:iterations) {
        		int[] ranges = new int[N];
            	//Arrays.fill(ranges, 2);
            	Algo RHC = new Algo("RHC");
            	Algo SA = new Algo("SA");
            	Algo GA = new Algo("GA");
            	Algo MIMIC = new Algo("MIMIC");
            	
            	for(int j=0; j<ii;j++) {
                	Random random = new Random();
                    // create the random points
                    double[][] points = new double[N][2];
                    for (int i = 0; i < points.length; i++) {
                        points[i][0] = random.nextDouble();
                        points[i][1] = random.nextDouble();   
                    }
                    
                    // for rhc, sa, and ga we use a permutation based encoding
                    TravelingSalesmanEvaluationFunction ef = new TravelingSalesmanRouteEvaluationFunction(points);
                    Distribution odd = new DiscretePermutationDistribution(N);
                    NeighborFunction nf = new SwapNeighbor();
                    MutationFunction mf = new SwapMutation();
                    CrossoverFunction cf = new TravelingSalesmanCrossOver(ef);
                    HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
                    GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
                    
                    long starttime = System.currentTimeMillis(); //new
                    RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);      
                    FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 100000);
                    fit.train();
                    //System.out.println(ef.value(rhc.getOptimal()));
                    RHC.perfList.add(ef.value(rhc.getOptimal()));
                    //System.out.println(rhc.hashCode());
                    //System.out.println(rhc.toString());
                    //System.out.println(rhc.getOptimizationProblem());
                    RHC.timeList.add(System.currentTimeMillis() - starttime);
                    //RHCline.xData.add(new Double(iterations[ii]));        	
                	//RHCTimeline.xData.add(new Double(iterations[ii]));
                	//RHC.printResults();            
                    //RHCline.yData.add(RHC.meanPerf());        
                    //RHCTimeline.yData.add(RHC.meanTime()/1000);
                    
                    starttime = System.currentTimeMillis();
                    SimulatedAnnealing sa = new SimulatedAnnealing(1E12, .95, hcp);
                    fit = new FixedIterationTrainer(sa, 100000);
                    fit.train();
                    //System.out.println(ef.value(sa.getOptimal()));
                    SA.perfList.add(ef.value(sa.getOptimal()));
                    SA.timeList.add(System.currentTimeMillis() - starttime);
                	//SAline.xData.add(new Double(iterations[ii]));
                	//SATimeline.xData.add(new Double(iterations[ii]));
                    //SA.printResults();
                    //SAline.yData.add(SA.meanPerf());
                    //SATimeline.yData.add(SA.meanTime()/1000);
                    
                    //if(iterations[ii] <= 10000) {
                    	 starttime = System.currentTimeMillis();
                         StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 20, gap);
                         fit = new FixedIterationTrainer(ga, 5000);
                         fit.train();
                         //System.out.println(ef.value(ga.getOptimal()));
                         GA.perfList.add(ef.value(ga.getOptimal()));
                         GA.timeList.add(System.currentTimeMillis() - starttime);
                     	//GAline.xData.add(new Double(iterations[ii]));
                     	//GATimeline.xData.add(new Double(iterations[ii]));
                         //GA.printResults();
                         //GAline.yData.add(GA.meanPerf());
                         //GATimeline.yData.add(GA.meanTime()/1000);
                    //}
                   
                    
                	//if(iterations[ii] <= 5000) {
                		starttime = System.currentTimeMillis();
                        // for mimic we use a sort encoding
                        ef = new TravelingSalesmanSortEvaluationFunction(points);
                        //int[] ranges = new int[N];
                        Arrays.fill(ranges, N);
                        odd = new  DiscreteUniformDistribution(ranges);
                        Distribution df = new DiscreteDependencyTree(.1, ranges); 
                        ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);
                       
                        MIMIC mimic = new MIMIC(200, 100, pop);
                        fit = new FixedIterationTrainer(mimic, 2000);
                        fit.train();
                        //System.out.println(ef.value(mimic.getOptimal()));
                        MIMIC.perfList.add(ef.value(mimic.getOptimal()));
                        MIMIC.timeList.add(System.currentTimeMillis() - starttime); 
                    	//MIMICline.xData.add(new Double(iterations[ii]));
                    	//MIMICTimeline.xData.add(new Double(iterations[ii]));
                        //MIMIC.printResults();
                        //MIMICline.yData.add(MIMIC.meanPerf());
                        //MIMICTimeline.yData.add(MIMIC.meanTime()/1000);
                	//}
                    
                    }
            	
            	chartit.addTimePointR(new Double(N), RHC.meanTime()/1000);
                chartit.addPointR(new Double(N), RHC.meanPerf());
                chartit.addTimePointS(new Double(N), SA.meanTime()/1000);
                chartit.addPointS(new Double(N), SA.meanPerf());
                chartit.addTimePointG(new Double(N), GA.meanTime()/1000);
                chartit.addPointG(new Double(N), GA.meanPerf());
                chartit.addTimePointM(new Double(N), MIMIC.meanTime()/1000);
                chartit.addPointM(new Double(N), MIMIC.meanPerf());
                
	        	RHC.printResults();
	            SA.printResults();
	            GA.printResults();
	            MIMIC.printResults();
            	
        		
        	}
        

        }
        
        chartit.addLines();
        chartit.chart(chartit.lines, fileName, "Traveling Sales Man", "N", "Fitness Performance");
        chartit.chart(chartit.Timelines, fileName+"-Time", "Traveling Sales Man", "N", "Seconds");
        
    }
    
    public static void Queens(String fileName) throws IOException {
    	//int N = 10;
    	//Test Variations of Queens: 4,8,10,20,50
    	//Time and Performance
    	
    	int[] Ns = new int[] {4,8,10,20,50};
    	
    	int[] iterations = new int[] { 500 };
    	Charty chartit = new Charty();
    	 
		//Do Multiple N values 
		for(int N: Ns) {
	        for(int ii: iterations) {
	        	int[] ranges = new int[N];
	        	Algo RHC = new Algo("RHC");
	        	Algo SA = new Algo("SA");
	        	Algo GA = new Algo("GA");
	        	Algo MIMIC = new Algo("MIMIC");
	        	
	        	for(int j=0; j<ii;j++) {
	            	Random random = new Random(N);
	                for (int i=0; i < N; i++) {
	                	ranges[i] = random.nextInt();
	                }
	                NQueensFitnessFunction ef = new NQueensFitnessFunction();
	                Distribution odd = new DiscretePermutationDistribution(N);
	                NeighborFunction nf = new SwapNeighbor();
	                MutationFunction mf = new SwapMutation();
	                CrossoverFunction cf = new SingleCrossOver();
	                Distribution df = new DiscreteDependencyTree(.1); 
	                HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
	                GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
	                ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);
	                
	                long starttime = System.currentTimeMillis();
	                RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);      
	                FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 100);
	                fit.train();
	                
	
	                //System.out.println("RHC: " + ef.value(rhc.getOptimal()));
	                //perf[0] = perf[0] + ef.value(rhc.getOptimal());
	                RHC.perfList.add(ef.value(rhc.getOptimal()));
	                RHC.timeList.add(System.currentTimeMillis() - starttime);
	                //System.out.println("RHC: Board Position: ");
	                //System.out.println(ef.boardPositions());
	                //System.out.println("Time : "+ (System.currentTimeMillis() - starttime));
	                //time[0] = time[0] + (System.currentTimeMillis() - starttime);
	               // System.out.println("============================");
	            	
	                
	                starttime = System.currentTimeMillis();
	                SimulatedAnnealing sa = new SimulatedAnnealing(1E1, .1, hcp);
	                fit = new FixedIterationTrainer(sa, 100);
	                fit.train();              
	                //System.out.println("SA: " + ef.value(sa.getOptimal()));
	                //perf[1] = perf[1] +  ef.value(sa.getOptimal());
	                //System.out.println("SA: Board Position: ");
	                // System.out.println(ef.boardPositions());
	                //System.out.println("Time : "+ (System.currentTimeMillis() - starttime));
	                //time[1] = time[1] + (System.currentTimeMillis() - starttime);
	                //System.out.println("============================");
	                SA.perfList.add(ef.value(sa.getOptimal()));
	                SA.timeList.add(System.currentTimeMillis() - starttime);
	            	
	
	                
	                starttime = System.currentTimeMillis();
	                StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 0, 10, gap);
	                fit = new FixedIterationTrainer(ga, 100);
	                fit.train();
	                //System.out.println("GA: " + ef.value(ga.getOptimal()));
	                //perf[2] = perf[2] +  ef.value(ga.getOptimal());
	                //System.out.println("GA: Board Position: ");
	                //System.out.println(ef.boardPositions());
	                //System.out.println("Time : "+ (System.currentTimeMillis() - starttime));
	                //time[2] = time[2] + (System.currentTimeMillis() - starttime);
	                //System.out.println("============================");
	                GA.perfList.add(ef.value(ga.getOptimal()));
	                GA.timeList.add(System.currentTimeMillis() - starttime);
	                
	
	                
	                starttime = System.currentTimeMillis();
	                MIMIC mimic = new MIMIC(200, 10, pop);
	                fit = new FixedIterationTrainer(mimic, 5);
	                fit.train();
	                //System.out.println("MIMIC: " + ef.value(mimic.getOptimal()));
	                //perf[3] = perf[3] + ef.value(mimic.getOptimal());
	                //System.out.println("MIMIC: Board Position: ");
	                //System.out.println(ef.boardPositions());
	                //System.out.println("Time : "+ (System.currentTimeMillis() - starttime));
	                //time[3] = time[3] + (System.currentTimeMillis() - starttime);
	                MIMIC.perfList.add(ef.value(mimic.getOptimal()));
	                MIMIC.timeList.add(System.currentTimeMillis() - starttime);
	                
	            }
	        	
	        	chartit.addTimePointR(new Double(N), RHC.meanTime()/1000);
                chartit.addPointR(new Double(N), RHC.meanPerf());
                chartit.addTimePointS(new Double(N), SA.meanTime()/1000);
                chartit.addPointS(new Double(N), SA.meanPerf());
                chartit.addTimePointG(new Double(N), GA.meanTime()/1000);
                chartit.addPointG(new Double(N), GA.meanPerf());
                chartit.addTimePointM(new Double(N), MIMIC.meanTime()/1000);
                chartit.addPointM(new Double(N), MIMIC.meanPerf());
                
	        	RHC.printResults();
	            SA.printResults();
	            GA.printResults();
	            MIMIC.printResults();
	            
	            
	        }
		}
        chartit.addLines();
        chartit.chart(chartit.lines, fileName, "N-Queens", "Queens", "Fitness Performance");
        chartit.chart(chartit.Timelines, fileName+"-Time", "N-Queens", "Queens", "Seconds");
     
    }
    

    
    private static void train(OptimizationAlgorithm oa, BackPropagationNetwork network, String oaName) {
        System.out.println("\nError results for " + oaName + "\n---------------------------");

        for(int i = 0; i < trainingIterations; i++) {
            oa.train();

            double error = 0;
            for(int j = 0; j < instances.length; j++) {
                network.setInputValues(instances[j].getData());
                network.run();

                Instance output = instances[j].getLabel(), example = new Instance(network.getOutputValues());
                example.setLabel(new Instance(Double.parseDouble(network.getOutputValues().toString())));
                error += measure.value(output, example);
            }

            //System.out.println(df.format(error));
        }
    }

    private static Instance[] initializeInstances() {

        double[][][] attributes = new double[4177][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src/abalone.txt")));

            for(int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[7]; // 7 attributes
                attributes[i][1] = new double[1];

                for(int j = 0; j < 7; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];

        for(int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            // classifications range from 0 to 30; split into 0 - 14 and 15 - 30
            instances[i].setLabel(new Instance(attributes[i][1][0] < 15 ? 0 : 1));
        }

        return instances;
    }
    
    private static Instance[] initializeInstancesForPhishing(int items, String filename, int attr) {

        double[][][] attributes = new double[items][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

            for(int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[attr]; // 7 attributes
                attributes[i][1] = new double[1];

                for(int j = 0; j < attr; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];

        for(int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            // classifications range from 0 to 30; split into 0 - 14 and 15 - 30
            instances[i].setLabel(new Instance(attributes[i][1][0] < 1 ? 0 : 1));
        }

        return instances;
    }
    
    public static void fourPeaks(String fileName) throws IOException {
    	int[] Ns = new int[] {50,100,200,500};
    	//int N = 200;
        
        Charty chartit = new Charty();
        //Vary the N and T?
        
        int[] iterations = new int[] {5};
    	 
        for(int N:Ns) {
        	int T = N / 5; //5 is 20% of total
        	
        	for(int ii:iterations) {
            	int[] ranges = new int[N];
            	Arrays.fill(ranges, 2);
            	Algo RHC = new Algo("RHC");
            	Algo SA = new Algo("SA");
            	Algo GA = new Algo("GA");
            	Algo MIMIC = new Algo("MIMIC");
            
            	
            	for(int j=0; j<ii;j++) {
            		EvaluationFunction ef = new FourPeaksEvaluationFunction(T);
                    Distribution odd = new DiscreteUniformDistribution(ranges);
                    NeighborFunction nf = new DiscreteChangeOneNeighbor(ranges);
                    MutationFunction mf = new DiscreteChangeOneMutation(ranges);
                    CrossoverFunction cf = new SingleCrossOver();
                    Distribution df = new DiscreteDependencyTree(.1, ranges); 
                    HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
                    GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
                    ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);
                    
                    long starttime = System.currentTimeMillis(); //new
                    
                    RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);      
                    FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 40000);
                    fit.train();
                    //System.out.println("RHC: " + ef.value(rhc.getOptimal()));
                    RHC.perfList.add(ef.value(rhc.getOptimal())); //new
                    RHC.timeList.add(System.currentTimeMillis() - starttime); //new
                    
                    starttime = System.currentTimeMillis();
                    SimulatedAnnealing sa = new SimulatedAnnealing(1E11, .95, hcp);
                    fit = new FixedIterationTrainer(sa, 60000);
                    fit.train();
                    //System.out.println("SA: " + ef.value(sa.getOptimal()));
                    SA.perfList.add(ef.value(sa.getOptimal()));
                    SA.timeList.add(System.currentTimeMillis() - starttime);
                    
                    starttime = System.currentTimeMillis();
                    StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 100, 10, gap);
                    fit = new FixedIterationTrainer(ga, 200000);
                    fit.train();
                    //System.out.println("GA: " + ef.value(ga.getOptimal()));
                    GA.perfList.add(ef.value(ga.getOptimal()));
                    GA.timeList.add(System.currentTimeMillis() - starttime);
                    
                    //if(iterations[ii] <= 10000) {
    	                starttime = System.currentTimeMillis();
    	                MIMIC mimic = new MIMIC(200, 20, pop);
    	                fit = new FixedIterationTrainer(mimic, 10000);
    	                fit.train();
    	                //System.out.println("MIMIC: " + ef.value(mimic.getOptimal()));
    	                MIMIC.perfList.add(ef.value(mimic.getOptimal()));
    	                MIMIC.timeList.add(System.currentTimeMillis() - starttime);
    	                
                    //}else {
                    //	MIMIC.perfList.add(200.0);
    	            //    MIMIC.timeList.add((long)0.0);
                    	
                    //}
                }
    	                
    	                chartit.addTimePointR(new Double(N), RHC.meanTime()/1000);
    	                chartit.addPointR(new Double(N), RHC.meanPerf());
    	                chartit.addTimePointS(new Double(N), SA.meanTime()/1000);
    	                chartit.addPointS(new Double(N), SA.meanPerf());
    	                chartit.addTimePointG(new Double(N), GA.meanTime()/1000);
    	                chartit.addPointG(new Double(N), GA.meanPerf());
    	                chartit.addTimePointM(new Double(N), MIMIC.meanTime()/1000);
    	                chartit.addPointM(new Double(N), MIMIC.meanPerf());
            	
            	RHC.printResults();
                SA.printResults();
                GA.printResults();
                MIMIC.printResults();
                
            }
        }
        
        
        
        chartit.addLines();
        chartit.chart(chartit.lines, fileName, "4-Peaks", "N", "Fitness Performance");
        chartit.chart(chartit.Timelines, fileName+"-Time", "4-Peaks", "N", "Seconds");
             
    }
    
}
